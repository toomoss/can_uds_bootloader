/**
 * @file   can_driver.c
 * @brief  CAN驱动程序
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-10-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include <stdint.h>
#include "main.h"
#include "can_boot.h"
#include "can_driver.h"
#include "can_uds.h"
#include "delay.h"
#include "can_buffer.h"
//CAN波特率参数表
CAN_BAUDRATE  CAN_BaudRateInitTab[]= {      
   {1000000,CAN_SJW_1tq,CAN_BS1_2tq,CAN_BS2_1tq,21},   // 1M
   {500000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,21},    // 500K
   {200000,CAN_SJW_1tq,CAN_BS1_4tq,CAN_BS2_1tq,70},    // 200K
   {100000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,105},   // 100K
   {50000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,210},    // 50K
   {20000,CAN_SJW_1tq,CAN_BS1_6tq,CAN_BS2_1tq,525},    // 20K
   {0,0,0,0,0}
};
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  从CAN波特率参数表里面获取CAN波特率参数
  * @param  BaudRate CAN波特率值，单位为bps
  * @param  pParaBaudRate CAN波特率参数结构体指针
  * @return 波特率参数获取状态状态
  * @retval 1 获取波特率参数成功
  * @retval 0 获取波特率参数失败
  */
uint8_t CAN_GetBaudRatePara(uint32_t BaudRate,CAN_BAUDRATE *pParaBaudRate)
{
  int i=0;
  while(CAN_BaudRateInitTab[i].BaudRate!=0){
    if(CAN_BaudRateInitTab[i].BaudRate==BaudRate){
      *pParaBaudRate = CAN_BaudRateInitTab[i];
      return 1;
    }
    i++;
  };
  return 0;
}

/**
  * @brief  CAN总线GPIO配置
  * @param  Channel CAN通道号
  * @retval None
  */
void CAN_GPIO_Configuration(uint8_t Channel)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  if(Channel==0){
    // CAN1 periph clock enable
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

    GPIO_PinAFConfig(GPIOB,GPIO_PinSource8,GPIO_AF_CAN1);
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource9,GPIO_AF_CAN1);
    // Configure CAN1 pin PB8->CAN1_RX  PB9->CAN1_TX
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
  }else{
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource12,GPIO_AF_CAN2);
    GPIO_PinAFConfig(GPIOB,GPIO_PinSource13,GPIO_AF_CAN2);
    // Configure CAN2 pin PB12->CAN2_RX  PB13->CAN2_TX
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
  }
}
/**
  * @brief  CAN接收数据中断配置
  * @param  Channel CAN通道号
  * @retval None
  */
void CAN_NVIC_Configuration(uint8_t Channel)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
  if(Channel==0){
    // Enable CAN1 RX0 interrupt IRQ channel
    NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
  }else{
    // Enable CAN2 RX0 interrupt IRQ channel
    NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX0_IRQn;
  }
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
/**
  * @brief  配置CAN接收过滤器
  * @param  FilterNumber 过滤器号
  * @param  ReceiveMsgId 接收数据的ID
	* @param  ReceiveMsgIdType 接收数据的ID类型，0-标准帧，1-扩展帧
  * @retval None
  */
void CAN_ConfigFilter(uint8_t Channel,uint8_t FilterNumber,uint32_t ReceiveMsgId,uint8_t ReceiveMsgIdType)
{
  CAN_FilterInitTypeDef  CAN_FilterInitStructure;

  //设置CAN接收过滤器
  CAN_FilterInitStructure.CAN_FilterNumber=Channel*14+FilterNumber;//过滤器1
  //CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;//屏蔽位模式
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdList;//标识符列表模式
  CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;//32bit模式
  //只接收消息类型为ReceiveMsgIdType，消息ID为ReceiveMsgId的帧
	if(ReceiveMsgIdType == CAN_Id_Standard){
		CAN_FilterInitStructure.CAN_FilterIdHigh=(ReceiveMsgId<<5)&0xFFFF;
		CAN_FilterInitStructure.CAN_FilterIdLow=0;
	}else{
		CAN_FilterInitStructure.CAN_FilterIdHigh=(((ReceiveMsgId>>18)&0x7FF)<<5)|((ReceiveMsgId>>13)&0x1F);
		CAN_FilterInitStructure.CAN_FilterIdLow=((ReceiveMsgId<<3)|((ReceiveMsgIdType&0X01)<<2))&0xFFFF;
	}
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment=0;
  CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;//使能过滤器
  CAN_FilterInit(&CAN_FilterInitStructure);	
}
/**
 * @brief  
 * @param  BaudRate: 
 */
void CAN_Configuration(uint8_t Channel,uint32_t BaudRate)
{
  CAN_BAUDRATE BaudRatePara;
  CAN_InitTypeDef  CAN_InitStructure;
  CAN_TypeDef* CAN[]={CAN1,CAN2};
  //初始化配置CAN中断
  CAN_NVIC_Configuration(Channel);
	//初始化配置CAN GPIO
  CAN_GPIO_Configuration(Channel);
	//初始化配置CAN
  CAN_StructInit(&CAN_InitStructure);
  CAN_InitStructure.CAN_TTCM = DISABLE;
  CAN_InitStructure.CAN_ABOM = DISABLE;
  CAN_InitStructure.CAN_AWUM = DISABLE;
  CAN_InitStructure.CAN_NART = DISABLE;
  CAN_InitStructure.CAN_RFLM = DISABLE;
  CAN_InitStructure.CAN_TXFP = ENABLE;
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	if(CAN_GetBaudRatePara(BaudRate,&BaudRatePara)){
    CAN_InitStructure.CAN_SJW = BaudRatePara.SJW;
    CAN_InitStructure.CAN_BS1 = BaudRatePara.BS1;
    CAN_InitStructure.CAN_BS2 = BaudRatePara.BS2;
    CAN_InitStructure.CAN_Prescaler = BaudRatePara.PreScale;
  }
  CAN_Init(CAN[Channel&0x01],&CAN_InitStructure);
  //设置CAN接收过滤器
  CAN_ConfigFilter(Channel,0,PHY_ADDR_ID,MSG_ID_TYPE);
  CAN_ConfigFilter(Channel,1,FUN_ADDR_ID,MSG_ID_TYPE);
  //使能接收中断，开始接收数据
  CAN_ITConfig(CAN[Channel&0x01],CAN_IT_FMP0, ENABLE);
}


/**
  * @brief  发送一帧CAN数据
  * @param  CANx CAN通道号
	* @param  TxMessage CAN消息指针
  * @retval 1-消息发送成功，0-消息发送失败
  */
uint8_t CAN_WriteData(uint8_t Channel,CAN_MSG *pMsg)
{
  CAN_TypeDef* CAN[]={CAN1,CAN2};
  uint8_t   TransmitMailbox;   
  uint32_t  TimeOut=0;
  //消息填充
  CanTxMsg TxMessage;
  if(pMsg->Flags.IDE){
    TxMessage.IDE = CAN_ID_EXT;
    TxMessage.ExtId = pMsg->ID;
  }else{
    TxMessage.IDE = CAN_ID_STD;
    TxMessage.StdId = pMsg->ID;
  }
  if(pMsg->Flags.RTR){
    TxMessage.RTR = CAN_RTR_REMOTE;
  }else{
    TxMessage.RTR = CAN_RTR_DATA;
  }
  TxMessage.DLC = (pMsg->Flags.DLC>8)?8:pMsg->Flags.DLC;
  memcpy(TxMessage.Data,pMsg->Data,TxMessage.DLC);

  TransmitMailbox = CAN_Transmit(CAN[Channel&0x01],&TxMessage);
  while(CAN_TransmitStatus(CAN[Channel&0x01],TransmitMailbox)!=CAN_TxStatus_Ok){
    TimeOut++;
    if(TimeOut > 100){
      return 0;
    }
    delay_ms(1);
  }
  return 1;
}
/**
  * @brief  CAN接收中断处理函数
  * @param  None
  * @retval None
  */
void CAN1_RX0_IRQHandler(void)
{
  CanRxMsg RxMessage;
  CAN_Receive(CAN1,CAN_FIFO0, &RxMessage);
  CAN_ClearITPendingBit(CAN1,CAN_IT_FMP0);
  CAN_MSG *pRxMsg = CAN_BUF_PickMsg(0);
	//对接收到的数据先做简单的判断，去掉错误的数据
#if MSG_ID_TYPE
  if((RxMessage.IDE == CAN_ID_EXT)&&((RxMessage.StdId == PHY_ADDR_ID)||(RxMessage.StdId == FUN_ADDR_ID))){
#else
  if((RxMessage.IDE == CAN_ID_STD)&&((RxMessage.StdId == PHY_ADDR_ID)||(RxMessage.StdId == FUN_ADDR_ID))){
#endif
    pRxMsg->Flags.IDE = (RxMessage.IDE == CAN_ID_STD)?0:1;
    pRxMsg->Flags.RTR = 0;
    pRxMsg->Flags.DIR = 0;
    pRxMsg->Flags.DLC = RxMessage.DLC;
    memcpy(pRxMsg->Data,RxMessage.Data,8);
    CAN_BUF_ApendMsg(0);
  }
}


/**
  * @brief  CAN接收中断处理函数
  * @param  None
  * @retval None
  */
void CAN2_RX0_IRQHandler(void)
{
  CanRxMsg RxMessage;
  CAN_Receive(CAN2,CAN_FIFO0, &RxMessage);
  CAN_ClearITPendingBit(CAN2,CAN_IT_FMP0);
  CAN_MSG *pRxMsg = CAN_BUF_PickMsg(1);
	//对接收到的数据先做简单的判断，去掉错误的数据
#if MSG_ID_TYPE
  if((RxMessage.IDE == CAN_ID_EXT)&&((RxMessage.StdId == PHY_ADDR_ID)||(RxMessage.StdId == FUN_ADDR_ID))){
#else
  if((RxMessage.IDE == CAN_ID_STD)&&((RxMessage.StdId == PHY_ADDR_ID)||(RxMessage.StdId == FUN_ADDR_ID))){
#endif
    pRxMsg->Flags.IDE = (RxMessage.IDE == CAN_ID_STD)?0:1;
    pRxMsg->Flags.RTR = 0;
    pRxMsg->Flags.DIR = 0;
    pRxMsg->Flags.DLC = RxMessage.DLC;
    memcpy(pRxMsg->Data,RxMessage.Data,8);
    CAN_BUF_ApendMsg(1);
  }
}

/*********************************END OF FILE**********************************/
