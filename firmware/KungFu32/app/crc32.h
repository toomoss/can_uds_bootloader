#ifndef __CRC32_H
#define __CRC32_H

uint32_t crc32(uint32_t crc, const uint8_t *buf, uint32_t len);

#endif
